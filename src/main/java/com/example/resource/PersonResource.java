package com.example.resource;

import com.example.models.City;
import com.example.models.Person;
import com.example.service.PersonService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

@Path("/")
public class PersonResource {
    @Inject
    PersonService service;

    @GET
    @Path("/person")
    @Produces(MediaType.TEXT_PLAIN)
    public Response addPerson(@QueryParam("id") Long id, @QueryParam("first_name") String first_name,
                              @QueryParam("last_name") String last_name, @QueryParam("age") int age,
                              @QueryParam("city") City city) {
        Person p = new Person(id, first_name, last_name, age, city);
        service.addPerson(p);
        NewCookie cookie = new NewCookie("hasCreated", "true");
        return Response.ok().cookie(cookie).build();
    }

//    @GET
//    @Path("/getPeople")
//    @Produces(MediaType.TEXT_PLAIN)
//    public List<Person> getPeople() {
//        return service.getPeople();
//    }
//
//    @GET
//    @Path("/getSortedPeople")
//    @Produces(MediaType.TEXT_PLAIN)
//    public List<Person> getSortedPeople() {
//        return service.getSortedPeople();
//    }
//
//    @GET
//    @Path("/getSortedMap")
//    @Produces(MediaType.TEXT_PLAIN)
//    public Map<String, City> getSortedMap() {
//        return service.getSortedMap();
//    }
//
//    @GET
//    @Path("/getLongestName")
//    @Produces(MediaType.TEXT_PLAIN)
//    public Person getLongestName() {
//        return service.getLongestName();
//    }
//
//    @GET
//    @Path("/getMinimumAge")
//    @Produces(MediaType.TEXT_PLAIN)
//    public Person getMinimumAge() {
//        return service.getMinimumAge();
//    }
    // . . .

}