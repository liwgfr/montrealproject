package com.example.resource;

import com.example.models.Child;
import com.example.service.ChildService;
import com.example.service.PersonService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
public class ChildResource {

    @Inject
    PersonService personService;
    ChildService childService;


    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/child")
    public Response addChild(@QueryParam("id") Long id, @QueryParam("email") String email) {
        Child child = new Child(personService.getPerson(id), email);
        childService.addChild(child);
        return Response.ok().build();
    }
}
