import java.util.ArrayList;
import java.util.Comparator;

public class ChildService {
    // todo implement all needed business-logic (just code)
    private ArrayList<Child> listOfChild = new ArrayList<>();

    public void addChild(Child child) {
        listOfChild.add(child);
        Log.debug("Child added");
    }

    public String eraseChild(Long id) {
        for (Child ch : listOfChild) {
            if (ch.getId().equals(id)) {
                listOfChild.remove(Child(getPerson(id)));
                return "Ok";
            }
        }
        return "Already clear";
    }

    private void getChildren() {
        listOfChild.sort(Comparator.comparing(x -> x.getId()));
        for (Child ch : listOfChild) {
            System.out.println(ch);
        }
    }
}