import java.util.ArrayList;
import java.util.Comparator;

public class PersonService {
    // todo implement all needed business-logic (just code)
    // Notice, you are eligible to write / test your code on your side.
    // It means you can do in your project all classes and then migrate them to the current structure
    private ArrayList<Person> listOfPeople = new ArrayList<>();

    public void addPerson(Person person) {
        if (listOfPeople.contains(person)) {
            throw new IllegalArgumentException("Person is already in the List");
        }
        listOfPeople.add(person);
        Log.debug("Person added");
    }

    public Person getPerson(Long id) {
        for (Person p : listOfPeople) {
            if (p.getId().equals(id)) {
                return p;
            } else {
                System.out.println("We don't have such person in our database");
            }
        }
        return null;
    }

    public String erasePerson(Long id) {
        for (Person p : listOfPeople) {
            if (p.getId().equals(id)) {
                listOfPeople.remove(getPerson(id));
                return "Ok";
            }
        }
        return "Already clear";
    }

    private void getPeople() {
        listOfPeople.sort(Comparator.comparing(x -> x.getId()));
        for (Person p : listOfPeople) {
            System.out.println(p);
        }
    }
}