public enum City {
    Auckland(1),
    Osaka(2),
    Adelaide(3),
    Wellington(4),
    Tokyo(5),
    Perth(6),
    Zurich(7),
    Geneva(8),
    Melbourne(9),
    Brisbane(10),
    Montreal(11),
    Berlin(12),
    Moscow(13),
    Stockholm(14),
    Minsk(15);

    private final int code;

    City(int code) {
        this.code = code;
    }

    public int getCode(City c) {
        return code;
    }
}