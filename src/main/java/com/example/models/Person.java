public class Person {
    // todo implement all needed methods
    private Long id;
    private String first_name;
    private String last_name;
    private int age;
    private City city;

    public Person(Long id, String first_name, String last_name, int age, City city) {
        this.id = id;
        if (first_name.matches("[;^?@!~*]") || first_name.length() < 2 || first_name.length() > 50) {
            throw new IllegalArgumentException ("Name must be 2-50 characters and may not contain ;^?@!~*");
        }
        this.first_name = first_name;
        if (last_name.matches("[;^?@!~*]") || last_name.length() < 2 || last_name.length() > 50) {
            throw new IllegalArgumentException ("Name must be 2-50 characters and may not contain ;^?@!~*");
        }
        this.last_name = last_name;
        if (age < 0 || age > 125) {
            throw new IllegalArgumentException ("The entered data may not be correct");
        }
        this.age = age;
        this.city = city;
    }

    public Person() {
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void getCity(String name) {
        for (City c : City.values()) {
            if (c.name().equals(name)) {
                System.out.println("City code is " + getCode(c));
            } else throw new IllegalArgumentException();
        }
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
