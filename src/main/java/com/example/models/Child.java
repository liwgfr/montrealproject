public class Child extends Person {
    // todo implement all needed methods
    private Person parent;
    private String email;

    public Child(Person parent, String email) {
        super();
        this.parent = parent;
        if (email == null || !email.matches("^(.+)@(.+)$"))
            throw new IllegalArgumentException("The Email address " + email +  " is invalid");
        this.email = email;
    }

    public Person getParent() {
        return parent;
    }

    public void setParent(Person parent) {
        this.parent = parent;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
