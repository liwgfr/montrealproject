## Project Plan

Notice, that you have to create an external branch for your development

### Idea

Create RESTful Web Application which supports such API usage:

- Server receives data by QueryParam from route **/person** and creates entity
- Server receives data by QueryParam from route **/child** and creates entity
- Server receives data by QuaryParam from route **/person** and gets back the entity
- Server responses all persons by route **/getPeople** the JSON of ArrayList<Person>
- Server responses sorted persons by last_name and age by route **/getSortedPeople** as JSON
- Server responses sorted persons by first_name as a pair of first_name and city array by route **/getSortedMap** as
  JSON
- Server responses person with the longest first_name + last_name by rote **/getLongestName** as JSON
- Server responses person with the lowest age by route **/getMinimumAge** as JSON
- Server clears all data of Person by route **/erasePerson** and responses String "Ok" if data exists and "Already clear" if no data was
  cleared
- Server clears all data of Child by route **/eraseChild** and responses String "Ok" if data exists and "Already clear" if no data was
  cleared
- Server responses all children by route **/getChildren** as JSON of ArrayList<Child>
- Server receives data by QueryParam and responses city code by name by route **/getCity** as a "City code is " +
  number;

## Entities

enum City: String name, int code

Person: Long id, String first_name, String last_name, int age, City city

Child extends Person, String email (check for valid)

## Roles

| @binocla | @dzimazilla |
| ----------- | ----------- |
| Create RESTful service | Create entities |
| Work out with JSON | Create methods in external class (not in models) | 
| Make Reactive | Write tests for methods | 
| Go Serverless | Handle unexpected exceptions | 
| Package & Deploy | Write JavaDoc | 
| Write tests for Paths |
| Write JavaDoc |

## Technology Stack

- [ ] AWS Lambda
- [ ] AWS Gateway
- [x] Maven 3.6.2+
- [x] Java 11+ (you can use Records classes)
- [ ] Quarkus Framework
- [ ] SmallRye Mutiny
- [ ] RESTEasy Jackson
- [ ] JUnit Framework (or similar)


